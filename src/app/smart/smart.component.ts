import { Component, OnInit, Input } from '@angular/core';
import { Aluno } from '../aluno';

@Component({
  selector: 'app-smart',
  templateUrl: './smart.component.html',
  styleUrls: [ './smart.component.css' ]
})
export class SmartComponent implements OnInit {

  pai = [];
  sucesso = false;
  aluno: Aluno;
  isValid = false;

  constructor() {
    this.aluno = new Aluno();
  }

  ngOnInit() { }

  enviar(): void {
    const a = this.aluno;
    const p = this.pai;

    this.sucesso = true;

    p.push(Object.assign({}, a));

    setTimeout(() => {
      this.sucesso = false;
    }, 1000);

    this.isValid = true;
  }


}
